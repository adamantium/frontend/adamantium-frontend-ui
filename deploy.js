'use strict';
const { execSync } = require('child_process');
const semver = require('semver');
const jsonfile = require('jsonfile');

// Logger
const log = {
  info: (msg) => console.log(msg),
  error: (err) => console.error(err)
};

// Release
const release = async () => {

  try {

    const packageJson = 'package.json';
    
    let pkgData = jsonfile.readFileSync(packageJson);
    let {version} = pkgData;
    version = semver.inc(version, 'patch');

    log.info(`Incrementing version to ${version}`);
    
    pkgData.version = version;
    jsonfile.writeFileSync(packageJson, pkgData, {spaces: 2});
        
    const tag = `release/v${version}`;
    execSync(`git tag -a ${tag} -m "Release ${tag}"`);
    
    execSync('git add --all');
    execSync(`git commit -m "Bump version to ${version}"`);  
    execSync('git push');
    execSync('git push --tags');

  } catch (err) {
    log.error(err);
    throw err;
  }

};

// Publish
const publish = async () => {

  try {
    const tag = execSync('git describe --tags --abbrev=0').toString().trim();
    const version = tag.split('/')[1].slice(1);

    log.info(`Publishing ${version}...`);
    
	execSync('npm publish');
  } catch (err) {
    log.error(err);
    throw err;
  }

};

(async () => {
  await release();
  await publish();
})();
