# Adamantium UI

Adamantium UI is a frontend Sass framework that enables the implementation of modern UI's based on design tokens. It is modular, simple, supports web components and the Shadow Dom. You can customize it to your own requirements. No old crap, just modern web standards without fallbacks. For high-performance UI's that take off.
**See the official documentation for more infos and examples. [Official Documentation](https://adamantium-ui.minddraft.com).**

## Requirements

Adamantium UI uses sass modules. You need mandatory dart sass in version 1.3.x.

## Installation

Just install it via npm.

```bash
npm install @minddraft/adamantium-frontend-ui
```

After installed, make sure you have at least 2 sass files in your project. One for the config to configure the tokens and one where you load all the sass modules from Adamantium UI.

```scss
_styles.scss
@use "sass:map";
@use "adamantium/config" as adm-cfg;
@use "/node_modules/@minddraft/adamantium-frontend-ui/lib/sass/core/utils/normalize";
@use "/node_modules/@minddraft/adamantium-frontend-ui/lib/sass/core" as adm;
@use "/node_modules/@minddraft/adamantium-frontend-ui/lib/sass/core/root";
```

```scss
_config.scss
//    Loading Sass Modules and Adamantium Core
@use "sass:color";
@use "sass:map";
@use "/node_modules/@minddraft/adamantium-frontend-ui/lib/sass/core" as adm;

//    Init loading Adamantium UI token files s
@use "/node_modules/@minddraft/adamantium-frontend-ui/lib/sass/core/tokens" as adm-tokens;
@use "/node_modules/@minddraft/adamantium-frontend-ui/lib/sass/core/assets" as adm-assets;
@use "/node_modules/@minddraft/adamantium-frontend-ui/lib/sass/core/component-tokens" as adm-component-tokens;

```

## Design Tokens in Adamantium

Design tokens are variables that represent design attributes such as colors, font sizes, spacing, and so on. They serve as a single source of truth for design attributes across different platforms and devices, enabling a more consistent user interface and a more efficient design system. In Adamantium UI, the Design Tokens are the fundament of every UI. You define the tokens in every Layer to archieve a flexibel, powerful and consistent frontend.

### Token Layers

In Adamantium UI, we currently have 3 token layers.

#### Primitives

Primitives tokens are the most basic tokens. They are the same across platforms and form the building blocks of the design, so to speak. They include, for example, dimensions, colors, fonts, etc. Primitives ALWAYS reference real values.

```scss
$primitive-tokens: (
	dimension: (
		1: 1,
		2: 2,

		4: 4,
		8: 8,

		12: 12,
		16: 16,
		20: 20,
		...,
	),
	color: (
		black: hsl(0, 0%, 0%),
		white: hsl(0, 0%, 100%),
		orange: hsl(36, 96%, 44%),
		purple-0: hsl(320, 10%, 92%),
		purple-30: hsl(320, 32%, 70%),
		purple-50: hsl(320, 32%, 26%),
		purple-60: hsl(320, 32%, 21%),
		...,
	),
	opacity: (
		20: 0.2,
		10: 0.1,
		50: 0.5,
		...,
	),
);
adm-tokens.$primitive-tokens: map.deep-merge(adm-tokens.$primitive-tokens, $primitive-tokens);
```

#### App Tokens

App tokens define "choices". What does my background look like, what does Brand Color look like, etc.? They are named semantically. They ALWAYS reference a primitive token. With App Tokens, there are 2 types of tokens. The Simple Tokens reference one value, the Composit Tokens reference multiple values, where the Key should always be a CSS property. Currently, only the type categorie uses composit tokens.

We have 12 default token categories, but you can add as many as you want. For all our default categories, we provide alias functions to access the values in a more efficient way

```scss
$app-tokens: (
	default: (
		// simple tokens
		app:
			(
				edge-padding: adm.to-rem(adm.primitive-token("dimension", 48)),
			),
		color: (
			surface-viewport: adm.primitive-token("color", purple-0),
			...,
		),
		space: (
			base: adm.to-rem(adm.primitive-token("dimension", 32)),
			small: adm.to-rem(adm.primitive-token("dimension", 24)),
			...,
		),
		grid: (
			columns: adm.primitive-token("dimension", 12),
			gap: adm.to-rem(adm.primitive-token("dimension", 32)),
			...,
		),
		height: (...),
		width: (
			small: adm.to-rem(adm.primitive-token("dimension", 512)),
			...,
		),
		breakpoint: (
			tablet: adm.to-px(adm.primitive-token("dimension", 768)),
			desktop: adm.to-px(adm.primitive-token("dimension", 1024)),
		),
		transition: (...),
		radius: (
			base: adm.to-rem(adm.primitive-token("dimension", 12)),
			small: adm.to-rem(adm.primitive-token("dimension", 8)),
			smaller: adm.to-rem(adm.primitive-token("dimension", 4)),
			...,
		),
		shadow: (...),
		border: (...),
		// composit tokens
		type:
			(
				h1: (
					font-size: adm.to-rem(adm.primitive-token("dimension", 48)),
					line-height: adm.to-rem(adm.primitive-token("dimension", 64)),
				),
				flow: (
					font-size: adm.to-rem(adm.primitive-token("dimension", 16)),
					,
					line-height: adm.to-rem(adm.primitive-token("dimension", 24)),
				),
			),
	),
	// additional modes here...
);
adm-tokens.$app-tokens: map.deep-merge(adm-tokens.$app-tokens, $app-tokens);
```

#### Component Tokens

Component tokens are the optional 3rd level. They reference app tokens or primitives and are defined for each component where it makes sense. The component tokens are named component-specifically and can be loaded optionally.

## Modes

In the app tokens, you have initially the default mode, wehre you definie your token categories and the tokens itself. You can also add as many additional modes as you want. Modes are only supported for the App Tokens. Different token values can be defined per mode. Important for the modes: ALL tokens must be defined in the default mode. In the other modes only the tokens which differ that differ from the default must be defined.

## Variations

Adamantium UI offers a variation system on component level. With this you can define several variations per component. For example, you can define a "narrow" variant for a list in addition to the standard version. Since components should be referenced to app tokens, components can be ideally combined with modes to allow flexible layouts.
